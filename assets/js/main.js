$(document).ready(() => {
    $('.image-container').css('height', $('.layer').css('width'));

    $('button').click(e => {
        $('#filter').css('background', $(e.target).val());
    });
});

$(window).resize(() => {
    $('.image-container').css('height', $('.layer').css('width'));
});